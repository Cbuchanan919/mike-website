
/* Not sure if these words should be included: opens, spends */
let ignore = ["filled", "painted", "unexpected", "bearded", "inclined", "good-natured", "need", "hunched"];
let pastTense = ["lived", "spent", "spends", "came", "comes", "were", "would", "was", "had", "said", "did", "didnt", "didn’t", "found"];
let bad = 'reknaw.tawt.seittit.stit.tihs.yssup.reggin.aggin.ffum.rekcufrehtom.niatszzij.lleh.nmaddog.gnikcuf.dekcuf.kcuf.daehkcid.kcid.nmad.tnuc.epiwmuc.rekcuskcoc.maofkcoc.kcoc.tihsllub.renob.hctib.hctaib.dratsab.boob.elohssa.ssa';
let oopsWords = ['Oops... ', 'Hmmm... ', 'Right... ', 'So... ', 'Oh dear... ', 'Whoops! ', 'Whoops!  Watch it there.  ']
let pronouns = ["me", "we", "my", "i"]
let oopsCounter = 0;
function getNextOopsWord() {
	if (oopsCounter >= oopsWords.length) { oopsCounter = 0; }
	oopsCounter += 1;
	return oopsWords[oopsCounter - 1]
}

function getPronounTriggered(word) {
	return 'Offending word: ' + word + '\n Triggered! Personal pronouns (e.g. I, me, my or we) in your quotation have not been edited in [square brackets] to match the rest of your sentence. (e.g. [she], [her], [they].)';
}

function getPastTenseTriggered(word) {
	return 'Triggered! (Offending word->' + word + '):\n Past tense verbs in your quotation have not been edited in [square brackets] to match the tense of the rest of your sentence.';
}

function getAlteredTenseTriggered(word) {
	return 'Triggered! (Offending word->' + word.toUpperCase() + '):\n One does not simply alter verb tenses in quotations without putting the altered word in [square brackets] to indicate one has done so.';
}

function getBadTriggered(word) {
	return getNextOopsWord() + ' The word ' + word.toUpperCase() + ' is not allowed. \n Please find a better word...\n\n';
}

function reverseString(str) {
	return str.split("").reverse().join("");
}
let bads = reverseString(bad).split('.');



const copyToClipboard = elements => {
	
	// let result = ''
	// for (let i = 0; i < elements.length; i++) {
	// 	result += elements[i].innerText + '\n\r';
	// }
	
	const el = document.createElement('textarea');
	el.style.visibility = 'none';
	el.value = elements.innerText;
	document.body.appendChild(el);
	el.select();
	document.execCommand('copy');
	document.body.removeChild(el);
	setTimeout(function(){ alert("Book report copied to clipboard."); }, 1000);
};



/* combines the necessary elements to validate and show errors */
class ElementInfo {
	constructor(elementId, elementName, starId, match, checkPronouns, checkTenses, alteredTenses) {

		if (typeof checkPronouns === 'undefined') { checkPronouns = false; }
		if (typeof checkTenses === 'undefined') { checkTenses = false; }
		if (typeof alteredTenses === 'undefined') { alteredTenses = ''; }
		if (typeof match === 'undefined') { match = ''; }

		this.element = document.getElementById(elementId);
		this.elementName = elementName;
		this.starElement = document.querySelector(starId);
		this.checkPronouns = checkPronouns;
		this.match = match.split("|");
		this.isValid = true;
		this.invalidReason = '';
		this.checkTenses = checkTenses;
		this.alteredTenses = alteredTenses.toLowerCase().split("|");
		this.configureTextboxWidth();
		this.errorColor = 'red';

		this.element.oninput = e => {
			if (this.elementOk(e)) {
				this.hideError();
				this.showSuccess();
			} else {
				this.showError();
			}
			this.configureTextboxWidth();
		}
	}

	/* This code checks to make sure the text entry is valid */
	elementOk(showError, alertError) {
		if (typeof showError === 'undefined') { showError = false; }
		if (typeof alertError === 'undefined') { alertError = false; }

		this.isValid = true;
		this.invalidReason = '';
		let testValue = this.element.value.trim();

		if (testValue == "") {
			this.isValid = false;
			this.invalidReason = getNextOopsWord() + this.elementName + ' is blank.';
		}


		if (this.isValid && this.match != '' && !this.match.includes(testValue)) {
			/* The element doesn't match one of the potential matches exactly. (It's not a match) */
			this.errorColor = 'red';

			for (let i = 0; i < this.match.length; i++) {
				const match = this.match[i];
				if (testValue.length <= match.length) {
					if (testValue == match.substring(0, testValue.length)) { this.errorColor = 'darkgoldenrod' }
				}

			}


			this.isValid = false;
			this.invalidReason = getNextOopsWord() + this.elementName + ' not entered correctly.';
		}

		if (this.isValid && this.checkPronouns) {
			this.processPronoun();
		}

		if (this.isValid && this.checkTenses) {
			// check past tense
			this.processTenses();
		}
		if (this.isValid) { this.processBad(); }

		if (!this.isValid) {
			if (showError == true) { this.showError(); }
			if (alertError == true) { this.alertError(); }
		}

		return this.isValid;
	}


	processPronoun() {

		if (this.element.value == '') {
			/* return blank. */
			this.isValid = false;
			this.invalidReason = "empty";
		} else {

			let ary = new Array(); ary = this.element.value.split(' ');

			for (var i = 0; i < ary.length; i++) {
				let testcase = ary[i].toLowerCase();
				if (pronouns.includes(testcase)) {
					this.isValid = false;
					this.invalidReason = getPronounTriggered(testcase);
				}
			}
		}
		return this.isValid;
	}

	processTenses() {

		/* this regex matches words that DON'T end in ed. */
		let nonEdEndingRegex = /^((?!ed$).)*$/;
		let presentTense = ["[are]"];

		/* checks the words against a list of past tense words */
		let words = new Array(); words = this.element.value.split(' ');
		let filteredWords = [];
		// console.log(this.alteredTenses);
		for (var i = 0; i < words.length; i++) {
			let testcase = words[i].toLowerCase().replace(',', '').replace('.', '').replace("!", "");

			if (ignore.includes(testcase)) {
				// do nothing
				console.log(testcase);
			} else {
				filteredWords.push(testcase); /* this line removes the 'ignore' past tense words  */
				if (pastTense.includes(testcase)) {
					this.isValid = false;
					this.invalidReason = getPastTenseTriggered(testcase);
				}
				else if (testcase != '' && this.alteredTenses.includes(testcase)) {
					this.isValid = false;
					this.invalidReason = getAlteredTenseTriggered(testcase);
				}
			}

		}

		if (this.isValid) {

			/* this is after the first past tense check to show irregular verb problems first */
			console.log(filteredWords);
			let unmatched = filteredWords.filter(RegExp.prototype.test, nonEdEndingRegex);
			if (unmatched.length != filteredWords.length) {
				this.isValid = false;
				this.invalidReason = "Your quotation contains past tense verbs. Please use [square brackets] to edit them into present tense to match your sentence."
			}
		}
		if (this.isValid && this.alteredTenses.length > 0) {
			/* checks for tenses that were improperly configured. */

		}
	}

	processBad() {
		for (let i = 0; i < bads.length; i++) {
			const bad = bads[i];
			if (this.element.value.indexOf(bad) != -1) {
				this.isValid = false;
				this.invalidReason = getBadTriggered(bad);
				return this.isValid;
			}
		}

	}


	configureTextboxWidth() {
		let width = (this.element.value.length >= this.element.placeholder.length + 3 ? this.element.value.length : this.element.placeholder.length + 3)
		// console.log(width, this.element.placeholder.length);
		if (width > 125) { width = 125; }
		this.element.style.minWidth = ((width + 1) * 8) + 'px';
	}



	showError() {
		this.element.style.color = 'white';
		this.element.style.background = this.errorColor;
		this.element.style.borderColor = 'red';
		this.element.style.boxShadow = "0px 0px 7px red";
		this.element.focus();

		this.starElement.style.display = 'inline';
	}

	showSuccess() {
		this.element.style.boxShadow = "0px 0px 7px green";
		this.element.style.borderColor = 'green';
	}

	hideError() {
		this.element.style.color = 'darkgreen';
		this.element.style.background = 'white';
		this.element.style.boxShadow = "";
		this.element.style.borderColor = 'darkgreen';
		this.starElement.style.display = 'none'
	}

	alertError() {
		if (this.invalidReason != '') {
			alert(this.invalidReason);
		}
	}

	value(usePlaceholder) {
		/* this returns the value of the element, or its placeholder */
		if (typeof usePlaceholder === 'undefined') { usePlaceholder = false; }
		return (usePlaceholder ? this.element.placeholder.trim() : this.element.value.trim());
	}

}