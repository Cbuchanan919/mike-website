/**
  * @param {String} url - address for the HTML to fetch
  * @return {String} the resulting HTML string fragment
  */
 async function fetchHtmlAsText(url) {
    return await (await fetch(url)).text();
}


/**
 * Load navigation to the navPlaceholder
 */
async function loadNav(backgroundColor, highlightID) {
    if (backgroundColor === undefined) {
        backgroundColor = "black";
    }
    
    const contentDiv = document.getElementById("navPlaceholder");
    contentDiv.innerHTML = await fetchHtmlAsText("/nav.html");

    let element = document.getElementById("siteNavigation")
    element.style.backgroundColor = backgroundColor;

    if (!((highlightID === undefined) || (highlightID === null))) {
        let highlightElement = document.getElementById(highlightID);
        highlightElement.style.backgroundColor = '#000000'
    }
}

